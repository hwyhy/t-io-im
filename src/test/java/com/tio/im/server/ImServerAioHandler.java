package com.tio.im.server;

import com.tio.im.common.ImPacket;
import com.tio.im.common.MessageHandler;
import org.tio.core.Aio;
import org.tio.core.ChannelContext;
import org.tio.core.intf.Packet;
import org.tio.server.intf.ServerAioHandler;

/**
 *
 * @author tanyaowu
 *
 */
public class ImServerAioHandler extends MessageHandler implements ServerAioHandler {
	/**
	 * 处理消息
	 */
	@Override
	public void handler(Packet packet, ChannelContext channelContext) throws Exception {
		ImPacket helloPacket = (ImPacket) packet;
		byte[] body = helloPacket.getBody();
		if (body != null) {
			String str = new String(body, ImPacket.CHARSET);
			System.out.println("收到消息：" + str);

			ImPacket resppacket = new ImPacket();
			resppacket.setBody(("收到了你的消息，你的消息是:" + str).getBytes(ImPacket.CHARSET));
			Aio.send(channelContext, resppacket);
		}
		return;
	}
}
