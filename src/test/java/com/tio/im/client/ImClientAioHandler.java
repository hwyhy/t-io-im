package com.tio.im.client;

import com.tio.im.common.ImPacket;
import com.tio.im.common.MessageHandler;
import org.tio.client.intf.ClientAioHandler;
import org.tio.core.ChannelContext;
import org.tio.core.intf.Packet;

/**
 *
 * @author tanyaowu
 *
 */
public class ImClientAioHandler extends MessageHandler implements ClientAioHandler {
	private static ImPacket heartbeatPacket = new ImPacket();

	/**
	 * 处理消息
	 */
	@Override
	public void handler(Packet packet, ChannelContext channelContext) throws Exception {
		ImPacket imPacket = (ImPacket) packet;
		byte[] body = imPacket.getBody();
		if (body != null) {
			String str = new String(body, ImPacket.CHARSET);
			System.out.println("收到消息：" + str);
		}

		return;
	}

	/**
	 * 此方法如果返回null，框架层面则不会发心跳；如果返回非null，框架层面会定时发本方法返回的消息包
	 */
	@Override
	public ImPacket heartbeatPacket() {
		return heartbeatPacket;
	}
}
