package com.tio.im.common;

/**
 * 2017年9月12日14:24:30
 */
public interface Const {

	public static final int PORT = 6666;	//端口号
	public static final int TIMEOUT = 5000;	//超时时间

}
