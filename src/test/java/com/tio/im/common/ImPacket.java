package com.tio.im.common;

import org.tio.core.intf.Packet;

/**
 * Created by hwy on 2017/9/12 0012.
 */
public class ImPacket extends Packet {
    public static final int HEADER_LENGHT = 4;//消息头的长度
    public static final String CHARSET = "utf-8";
    private byte[]  body;    //消息体

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }
}
