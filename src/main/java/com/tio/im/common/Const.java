package com.tio.im.common;

/**
 *
 * @author tanyaowu
 *
 */
public interface Const {

	public static final int PORT = 6666;

	public static final String CHARSET = "utf-8";

	public static final int TIMEOUT = 5000;

}
