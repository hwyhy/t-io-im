package com.tio.im.client;

import com.tio.im.common.Const;
import com.tio.im.common.ShowcasePacket;
import com.tio.im.common.Type;
import com.tio.im.common.packets.GroupMsgReqBody;
import com.tio.im.common.packets.JoinGroupReqBody;
import org.tio.client.AioClient;
import org.tio.client.ClientChannelContext;
import org.tio.client.ClientGroupContext;
import org.tio.client.ReconnConf;
import org.tio.client.intf.ClientAioHandler;
import org.tio.client.intf.ClientAioListener;
import org.tio.core.Aio;
import org.tio.core.Node;
import org.tio.json.Json;

/**
 *	群聊
 * @author tanyaowu
 */
public class GroupMsgClientStarter {
	static String serverIp = "127.0.0.1";
	static int serverPort = Const.PORT;

	private static Node serverNode = new Node(serverIp, serverPort);

	//用来自动连接的，不想自动连接请设为null
	private static ReconnConf reconnConf = new ReconnConf(5000L);

	private static ClientAioHandler aioClientHandler = new ShowcaseClientAioHandler();
	private static ClientAioListener aioListener = new ShowcaseClientAioListener();
	private static ClientGroupContext clientGroupContext = new ClientGroupContext(aioClientHandler, aioListener, reconnConf);

	private static AioClient aioClient = null;

	static ClientChannelContext clientChannelContext;

	public static void main(String[] args) throws Exception {
		aioClient = new AioClient(clientGroupContext);
		clientChannelContext = aioClient.connect(serverNode);
		command();
	}

	public static void command() throws Exception {
		String group = "group1";
		String text = "你好";
		processCommand(group, text);
		aioClient.stop();
		System.exit(0);
	}

	public static void processCommand(String group, String text) throws Exception {
		GroupMsgReqBody groupMsgReqBody = new GroupMsgReqBody();
		groupMsgReqBody.setToGroup(group);
		groupMsgReqBody.setText(text);

		ShowcasePacket reqPacket = new ShowcasePacket();
		reqPacket.setType(Type.GROUP_MSG_REQ);
		reqPacket.setBody(Json.toJson(groupMsgReqBody).getBytes(ShowcasePacket.CHARSET));

		Aio.send(clientChannelContext, reqPacket);
	}
}
