package com.tio.im.client;

import com.tio.im.common.Const;
import com.tio.im.common.ShowcasePacket;
import com.tio.im.common.Type;
import com.tio.im.common.packets.GroupMsgReqBody;
import com.tio.im.common.packets.JoinGroupReqBody;
import com.tio.im.common.packets.LoginReqBody;
import com.tio.im.common.packets.P2PReqBody;
import org.apache.commons.lang3.StringUtils;
import org.tio.client.AioClient;
import org.tio.client.ClientChannelContext;
import org.tio.client.ClientGroupContext;
import org.tio.client.ReconnConf;
import org.tio.client.intf.ClientAioHandler;
import org.tio.client.intf.ClientAioListener;
import org.tio.core.Aio;
import org.tio.core.Node;
import org.tio.json.Json;

/**
 *	登录
 * @author tanyaowu
 */
public class LoginClientStarter {
	static String serverIp = "127.0.0.1";
	static int serverPort = Const.PORT;

	private static Node serverNode = new Node(serverIp, serverPort);

	//用来自动连接的，不想自动连接请设为null
	private static ReconnConf reconnConf = new ReconnConf(5000L);

	private static ClientAioHandler aioClientHandler = new ShowcaseClientAioHandler();
	private static ClientAioListener aioListener = new ShowcaseClientAioListener();
	private static ClientGroupContext clientGroupContext = new ClientGroupContext(aioClientHandler, aioListener, reconnConf);

	private static AioClient aioClient = null;

	static ClientChannelContext clientChannelContext;

	public static void main(String[] args) throws Exception {
		aioClient = new AioClient(clientGroupContext);
		clientChannelContext = aioClient.connect(serverNode);
		command();
	}

	public static void command() throws Exception {
		String loginname = "hwy";
		String password = "123456";
		while (true){
			processCommand(loginname, password);
			Thread.sleep(2000);
		}

//		aioClient.stop();
//		System.exit(0);
	}

	public static void processCommand(String loginname, String password) throws Exception {

		LoginReqBody loginReqBody = new LoginReqBody();
		loginReqBody.setLoginname(loginname);
		loginReqBody.setPassword(password);

		ShowcasePacket reqPacket = new ShowcasePacket();
		reqPacket.setType(Type.LOGIN_REQ);
		reqPacket.setBody(Json.toJson(loginReqBody).getBytes(ShowcasePacket.CHARSET));

		Aio.send(clientChannelContext, reqPacket);
	}
}
